FROM ubuntu
RUN  apt update -y && apt install -y python3-pip python3-dev
COPY ./requirements.txt /app/requirements.txt
WORKDIR /app
RUN pip install --upgrade pip && pip install -r requirements.txt
COPY . /app/
ENTRYPOINT ["python" ]
CMD [ "app.py" ]